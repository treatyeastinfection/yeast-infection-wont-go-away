ODDS ARE PRETTY HIGH that you're familiar with the medical condition known as a yeast infection, as statistics suggest that up to 75 percent of all American women contract a vaginal yeast infection (along with the accompanying itching and thick, discolored discharge) at least once in their lives. But what you may not know is that the official medical term for a yeast infection is candidiasis, and adult women are not the only ones susceptible to the condition.

Candidiasis refers to an overgrowth of the yeast Candida albicans as shown here http://healtheline.info/yeast-infection-wont-go-away/ . At normal levels, Candida is harmless and exists alongside the trillions of other bacteria in the mouth, vagina, rectum and digestive tracts. But if there's an overgrowth, candida can lead to significant health problems – in men, women and even children. Sore throats and those pesky yeast infections typically affect adults, while babies are more likely to develop oral thrush. Leaky gut is a more serious consequence of candidiasis, occurring when Candida causes the intestinal wall to become permeable and allows partially digested proteins and other toxins to be released into the body. And in severe cases, known as invasive candidiasis, Candida can negatively impact vital parts of the body, including the blood, heart and brain – ultimately leading to hospitalization and, rarely, death.


The good news is that, once you become aware of the early symptoms of Candida overgrowth, you can take the appropriate steps to treat Candida naturally and avoid all of the aforementioned conditions.

 Symptoms of Candida Overgrowth

So what are some of the signs that Candida is running rampant in your body? Let's take a look at five symptoms that may indicate a problem.

Mood disorders

Scientists are still uncovering just how connected our brains and digestive systems are, but we do know that if something is not functioning properly in the gut (say, if there's an overgrowth of bad bacteria), it can most certainly affect the neurological system. As a result, anxiety, panic attacks, depression, dramatic mood swings and irritability may all point to Candida overgrowth.


Chronic fatigue

Chronic fatigue syndrome is characterized by sheer exhaustion that cannot be overcome. More specifically, it's defined as fatigue lasting no less than six months that's accompanied by additional symptoms such as joint pain, brain fog and headaches, to name a few. For those with Candida overgrowth, chronic fatigue is often one of the symptoms identified.

Gastrointestinal issues

When symptoms such as bloating, gas, cramps and either diarrhea or constipation are present, it's a clear signal that something is amiss in your digestive tract. One cause may well be insufficient levels of healthy bacteria, which could have been caused by Candida overgrowth.

Sinus infections

An excessive amount of Candida can wreak havoc on the sinuses, causing sore throat, congestion, allergies, post-nasal drip and other symptoms of sinus upset.
